from dfhack.rpc.rpcfunction import RemoteFunctionBase
from dfhack.rpc import RPCClient
from dfhack.rpc.proto.dfhack_core_pb2 import EmptyMessage
from dfhack.rpc.proto.dfhack_remotefortressreader_pb2 import UnitList

class ConnectionState(object):
    def __init__(self, address, port):

        rpc = RPCClient(address, port)
        rpc.connect()

        #UnitListCall = new RemoteFunction<dfproto.EmptyMessage, RemoteFortressReader.UnitList>();
        #UnitListCall.bind(network_client, "GetUnitList", "RemoteFortressReader");
        self.UnitListCall = RemoteFunctionBase(EmptyMessage, UnitList)
        rpc.bind(self.UnitListCall, 'GetUnitListInside', 'RemoteFortressReader')

    def GetUnitList(self):
        cr, unitList = self.UnitListCall.TryExecute()
        for creature in unitList.creature_list:
            print(creature)
