import logging
import socket

from dfhack.rpc.proto.dfhack_core_pb2 import (CoreBindReply, CoreBindRequest,
                                              CoreRunCommandRequest,
                                              EmptyMessage, CoreTextNotification)
from dfhack.rpc.proto.magic import (HANDSHAKE_REQUEST, HANDSHAKE_RESPONSE,
                                    ECommandResponse, EDFHackReplyCode)
from dfhack.rpc.proto.structs import RPCMessageHeader, RPCHandshakeRequest
from dfhack.rpc.rpcfunction import RemoteFunctionBase


class RPCClient(object):
    log = logging.getLogger('RPCClient')

    def __init__(self, address, port):
        self.address = address
        self.port = port

        self.socket = None
        self.active = False

        self.bind_call = RemoteFunctionBase(CoreBindRequest, CoreBindReply)
        self.bind_call.name = 'BindMethod'
        self.bind_call.remote_client = self
        self.bind_call.ID = 0

        self.runcmd_call = RemoteFunctionBase(CoreRunCommandRequest, EmptyMessage)
        self.runcmd_call.name = 'RunCommand'
        self.runcmd_call.remote_client = self
        self.runcmd_call.ID = 1

    def connect(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.address, self.port))

        self.send(RPCHandshakeRequest().serialize())
        response = self.read()
        assert response == HANDSHAKE_RESPONSE
        self.log.info('Handshake success!')
        self.active=True


    def sendMessage(self, _id, message, out_message=None):
        if out_message is None:
            out_message = EmptyMessage()
        if self.socket is None:
            self.log.error("Cannot send message: Invalid socket")
            return (ECommandResponse.CR_LINK_FAILURE, None)
        msgbytes = message.SerializeToString()

        header = RPCMessageHeader()
        header.ID = _id
        header.Size = len(msgbytes)
        self.send(header.serialize())
        self.send(message.SerializeToString())

        self.recvMessage(out_message)

    def recvMessage(self, out):
        while True:
            header = RPCMessageHeader()
            buffer = self.read(8)
            if len(buffer)<8:
                self.log.error("I/O error in receive header.")
                return ECommandResponse.CR_LINK_FAILURE
            header.deserialize(buffer)

            self.log.info("Received %r:%r.", header.ID, header.Size)

            if header.ID == EDFHackReplyCode.RPC_REPLY_FAIL:
                if header.Size == int(ECommandResponse.CR_OK):
                    return ECommandResponse.CR_FAILURE
                else:
                    return ECommandResponse(header.Size)

            if header.Size < 0 or header.Size > RPCMessageHeader.MAX_MESSAGE_SIZE:
                self.log.error('Invalid size returned by header.')
                return ECommandResponse.CR_LINK_FAILURE

            if header.ID == int(EDFHackReplyCode.RPC_REPLY_RESULT):
                buffer = self.read(header.Size)
                out.ParseFromString(buffer)
                return ECommandResponse.CR_OK

            elif header.ID == int(EDFHackReplyCode.RPC_REPLY_TEXT):
                buffer = self.read(header.Size)
                ctn = CoreTextNotification()
                ctn.ParseFromString(buffer)
                for frag in ctn.fragments:
                    self.log.info(f"<{frag.color}>{frag.text}</{frag.color}>")
                return ECommandResponse.CR_NOT_IMPLEMENTED

    def send(self, data: bytes):
        self.log.info("O > %s", data)
        self.socket.sendall(data)

    def read(self, size=1024):
        buffer = b''
        reply_size = 0
        last_reply_size = 0
        i = 0
        while True:
            i += 1
            buf = self.socket.recv(size)
            last_reply_size = len(buffer)
            buffer += buf
            reply_size += last_reply_size
            #print(i,last_reply_size)
            if last_reply_size < size:
                break

        self.log.info("I < %s", buffer)
        return buffer

    def bind(self, function, name, plugin):
        if self.socket is None or not self.active:
            return False

        self.bind_call.reset()

        inmsg = self.bind_call.get_in()
        inmsg.method = name
        inmsg.plugin = plugin
        inmsg.input_msg = function.in_template.__name__
        inmsg.output_msg = function.out_template.__name__

        res, msg = self.bind_call.TryExecute()
        if res != ECommandResponse.CR_OK:
            return False

        function.ID = self.bind_call.output.assigned_id
        return True

    def RunCommand(self, command, args=None):
        if args is None:
            args = []
        if not self.active or self.socket:
            return ECommandResponse.CR_FAILURE

        self.runcmd_call.reset()

        self.runcmd_call.input.command = command
        for arg in args:
            self.runcmd_call.input.arguments.append(arg)

        return self.runcmd_call.TryExecute()
