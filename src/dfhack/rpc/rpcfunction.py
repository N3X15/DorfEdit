from typing import Any
from dfhack.rpc.proto.dfhack_core_pb2 import EmptyMessage
from dfhack.rpc.proto.magic import ECommandResponse
import logging

class RPCFunctionBase(object):
    def __init__(self, in_template, out_template=EmptyMessage):
        self.in_template = in_template
        self.out_template = out_template

        self._in=None
        self._out=None

        self.name = ''
        self.proto = ''

    def make_in(self) -> Any:
        return self.in_template()

    def get_in(self) -> Any:
        if self._in is None:
            self._in = self.make_in()
        return self._in

    def make_out(self) -> Any:
        return self.out_template()

    def get_out(self) -> Any:
        if self._out is None:
            self._out = self.make_out()
        return self._out

    def reset(self, free: bool = False) -> None:
        if free:
            self._in = None
            self._out = None
        else:
            if self._in is not None:
                self._in = self.make_in()
            if self._out is not None:
                self._out = self.make_out()

class RemoteFunctionBase(RPCFunctionBase):
    log = logging.getLogger('RemoteFunctionBase')
    def __init__(self, in_template, out_template):
        super().__init__(in_template, out_template)
        self.ID=-1
        self.remote_client = None

    def is_valid(self):
        return self.ID >= 0

    def TryExecute(self):
        if not self.is_valid():
            self.log.error("Cannot execute unbound function %s::%s", self.proto, self.name)
            return (ECommandResponse.CR_NOT_IMPLEMENTED, None)
        return self.remote_client.sendMessage(self.ID, self.get_in(), self.get_out())

    def bind(self, remote_client, name, proto=""):
        if self.is_valid():
            if self.remote_client == remote_client and self.name == name and self.proto == proto:
                return True
            self.log.error("Function already bound to %s::%s", self.proto, self.name)
            return False
        self.remote_client=remote_client
        self.name=name
        self.proto=proto

        self.remote_client.bind(self, name, proto)
