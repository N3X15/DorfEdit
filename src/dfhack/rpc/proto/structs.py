import struct
import typing
import io

class CStruct(object):
    '''
    A mess of code for building a C-struct (de)serializer.
    '''
    FIELDS = []

    def serialize(self) -> typing.Tuple[int, bytes]:
        o=b''
        sz = 0
        for attr,ty in self.FIELDS:
            _sz,_o = self.serialize_field(attr, ty)
            sz += _sz
            o += _o
        return o

    def serialize_field(self, attr: str, typeID: str) -> typing.Tuple[int, bytes]:
        sz = struct.calcsize(typeID)
        o=b''
        if attr == '' and typeID.endswith('x'):
            o += b'\0' * (int(typeID[:-1])+1)
        else:
            o = struct.pack(typeID, getattr(self, attr))
        return sz, o


    def deserialize(self, stream):
        if isinstance(stream, bytes):
            stream = io.BytesIO(stream)
        for attr,ty in self.FIELDS:
            self.deserialize_field(stream, attr, ty)

    def deserialize_field(self, stream, attr, typeID):
        sz = struct.calcsize(typeID)
        buf = stream.read(sz)
        if attr != '':
            setattr(self, attr, struct.unpack(typeID, buf)[0])

class RPCHandshakeRequest(CStruct):
    '''
    struct RPCHandshakeHeader {
        char magic[8];
        int version;

        static const char REQUEST_MAGIC[9];
        static const char RESPONSE_MAGIC[9];
    };
    '''
    FIELDS = [
        ('Magic', '8s'),
        ('Version', 'i')
    ]

    def __init__(self):
        self.Magic=b'DFHack?\n'
        self.Version = 1

class RPCMessageHeader(CStruct):
    '''
    struct RPCMessageHeader {
        static const int MAX_MESSAGE_SIZE = 64*1048576;

        int16_t id;
        // char[2] slop;
        int32_t size;
    };
    '''
    MAX_MESSAGE_SIZE = 64*1048576
    FIELDS = [
        ('ID',   'h'),
        ('',     '1x'), # 2 padding bytes
        ('Size', 'I')
    ]

    def __init__(self):
        self.ID = 0
        self.Size = 0
