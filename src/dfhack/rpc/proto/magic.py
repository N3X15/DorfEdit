from enum import IntEnum
HANDSHAKE_REQUEST = b"DFHack?\n\x01\x00\x00\x00"
HANDSHAKE_RESPONSE = b"DFHack!\n\x01\x00\x00\x00"

class ECommandResponse(IntEnum):
    CR_LINK_FAILURE = -3     # RPC call failed due to I/O or protocol error
    CR_NEEDS_CONSOLE = -2    # Attempt to call interactive command without console
    CR_NOT_IMPLEMENTED = -1  # Command not implemented, or plugin not loaded
    CR_OK = 0                # Success
    CR_FAILURE = 1           # Failure
    CR_WRONG_USAGE = 2       # Wrong arguments or ui state
    CR_NOT_FOUND = 3         # Target object not found (for RPC mainly)

class EDFHackReplyCode(IntEnum):
    RPC_REPLY_RESULT = -1,
    RPC_REPLY_FAIL = -2,
    RPC_REPLY_TEXT = -3,
    RPC_REQUEST_QUIT = -4
