import os
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.fileio import CopyFileTarget, ReplaceTextTarget

from buildtools import os_utils, log

class ProtoCBuildTarget(SingleBuildTarget):
    BT_TYPE = 'PROTOC'
    def __init__(self, outdir, infile, protopath, dependencies=[], provides=[], name=None):
        self.outdir = os.path.abspath(outdir)
        self.indir = os.path.abspath(os.path.dirname(infile))
        self.proto_path = os.path.abspath(protopath)
        basename = os.path.basename(infile)
        basename, ext = os.path.splitext(basename)
        self.outfile=os.path.join(self.outdir, basename+'_pb2.py')
        self.infile=os.path.abspath(infile)
        super().__init__(target=self.outfile, files=[infile], dependencies=dependencies, provides=provides, name=name or os.path.relpath(self.outfile))

    def build(self):
        with os_utils.Chdir(self.indir, quiet=True):
            os_utils.cmd(['protoc','-I='+self.proto_path, '--python_out='+self.outdir, self.infile], echo=False, show_output=True, critical=True, globbify=False)
        self.touch(self.outfile)


def main():
    bm = BuildMaestro()

    #core_proto = bm.add(CopyFileTarget(
    #    os.path.join('tmp', 'dfhack_core.proto'),
    #    os.path.join('lib', 'dfhack', 'library', 'proto', 'CoreProtocol.proto'),
    #    ))
    core_proto = bm.add(ReplaceTextTarget(
        target=os.path.join('tmp', 'dfhack_core.proto'),
        filename=os.path.join('lib', 'dfhack', 'library', 'proto', 'CoreProtocol.proto'),
        replacements={
            r'(option optimize_for = .*)$': '/* DorfEdit: Not needed. - \\1 */',
        }))
    bm.add(ProtoCBuildTarget(
        outdir=os.path.join('src', 'dfhack', 'rpc', 'proto'),
        infile=core_proto.target,
        protopath=os.path.join('tmp'),
        dependencies=[core_proto.target]))

    rfr_proto = bm.add(ReplaceTextTarget(
        target=os.path.join('tmp', 'dfhack_remotefortressreader.proto'),
        filename=os.path.join('lib', 'dfhack', 'plugins', 'proto', 'RemoteFortressReader.proto'),
        replacements={
            r'(option optimize_for = .*)$': '/* DorfEdit: Not needed. - \\1 */',
        }))
    bm.add(ProtoCBuildTarget(
        outdir=os.path.join('src', 'dfhack', 'rpc', 'proto'),
        infile=rfr_proto.target,
        protopath=os.path.join('tmp'),
        dependencies=[rfr_proto.target]))
    bm.as_app()
if __name__ == '__main__':
    main()
